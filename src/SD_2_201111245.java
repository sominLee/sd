package com.ex.ood;

import com.ex.ood.*;
import java.util.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class SD_2_201111245 {
	public static void main(String [] args){
		ch7main();
		//ch5main();
		//ch4main();
		//ch3main();
		//ch2main();
	}

	public static void ch7main(){

		System.out.println("--ch7 Factory Pattern");

		Ch7SimplePizzaFactory pizzaF = new Ch7SimplePizzaFactory();
		pizzaF.createPizza("cheese");
		Ch7PizzaStore store = new Ch7PizzaStore(pizzaF);
		store.orderPizza("cheese");
		
		System.out.println("--ch7 Command Pattern ");
	
		Ch7SimpleRemoteControl control =new Ch7SimpleRemoteControl();
		Ch7Light light = new Ch7Light();
		Ch7LightOnCommand lighton = new Ch7LightOnCommand(light);
		Ch7LightOffCommand lightoff = new Ch7LightOffCommand(light);
		Ch7GarageDoor garage = new Ch7GarageDoor();
		Ch7GarageDoorOpenCommand garagedoor = new Ch7GarageDoorOpenCommand(garage);
		Ch7GarageDoorDownCommand garagedoordown = new Ch7GarageDoorDownCommand (garage);

		control.setCommand(lighton);
		control.buttonWasPressed();
		control.setCommand(lightoff);
		control.buttonWasPressed();
		control.setCommand(garagedoor);
		control.buttonWasPressed();
		control.setCommand(garagedoordown);
                control.buttonWasPressed();


		System.out.println("--ch7 SingPatternLoggerBefore ");
		Ch7SingPatternLoggerBefore pattern = new Ch7SingPatternLoggerBefore();
		pattern.writeLine("a");
		pattern.readEntireLog();

		System.out.println("--ch7 SingPatternLogger ");
 		Ch7SingPatternLogger pattern1 = Ch7SingPatternLogger.getInstance();
		pattern1.readEntireLog();
		
}
	public static void ch5main(){
	
		System.out.println("--ch5 List ");
		
		Ch5ArrayLinkedList linkedlist = new Ch5ArrayLinkedList ();
		linkedlist.printArrayList();
		linkedlist.printLinkedList();

		System.out.println("--ch5 pattern ");

		Ch5ObsPatternView view = new Ch5ObsPatternView();
                Ch5ObsPatternModel model = new Ch5ObsPatternModel();
                model.addObserver(view);
                model.changeSomething();
	
		System.out.println("--ch5 FixedPoint _immutable");
		System.out.println("--version1");
		
		Ch5FixedPointv1New a= new Ch5FixedPointv1New(1,3);
		System.out.println(a.getX()+","+a.getY());
	//	a.x = 2;
	//	a.y = 4;
	//	System.out.println(a.getX()+","+a.getY());
	//	private => immutable
		System.out.println("--version2");		
		Ch5FixedPointv2Inheritance b = new Ch5FixedPointv2Inheritance(1,3);
		System.out.println(b.getX()+","+b.getY());
		b.x = 2;
                b.y = 4;
                System.out.println(b.getX()+","+b.getY());
	//  	public => mutable

		System.out.println("--version3");
		Ch5FixedPointv3Association c = new Ch5FixedPointv3Association(new Point(1,3));
		System.out.println(c.getX()+","+c.getY());
		System.out.println(c.getLocation());
	//	c.pt.x = 2;
        //      c.pt.y = 4;
        //      System.out.println(c.getX()+","+c.getY());
	//	private => immutable
	// 	mutable => getLocation()return pt - Point a= new Point(1,2) -change possible 		
}
	public static void ch4main(){
		System.out.println("--ch4 Equals");
                System.out.println("--Equals _ version 1");

Ch4Triangle1 t = new Ch4Triangle1(new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle1 ct = new Ch4ColoredTriangle1(Color.red,new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle1 ct2 = new Ch4ColoredTriangle1(Color.blue,new Point(0,0),new Point(1,1),new Point(2,2));
	
		System.out.println("<reflexive>");
		System.out.println(t.equals(t));
		System.out.println(ct.equals(ct));
		System.out.println("<symmetric>");
		System.out.println(t.equals(ct));
		System.out.println(ct.equals(t));
		System.out.println("<transitive>");
		System.out.println(ct.equals(t));
		System.out.println(t.equals(ct2));
		System.out.println(ct.equals(ct2));
		System.out.println("<consistent>");
		System.out.println(t.equals(ct));
		System.out.println("<non-null>");
		System.out.println(t.equals(null)); 
		System.out.println(ct.equals(null));

		System.out.println("--Equals _ version 2");

Ch4Triangle1 t2 = new Ch4Triangle1(new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle2 rct = new Ch4ColoredTriangle2(Color.red,new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle2 bct = new Ch4ColoredTriangle2(Color.blue,new Point(0,0),new Point(1,1),new Point(2,2));

		System.out.println("<reflexive>");
                System.out.println(t2.equals(t2));
                System.out.println(rct.equals(rct));
		System.out.println(bct.equals(bct));
                System.out.println("<symmetric>");
                System.out.println(t2.equals(rct));
                System.out.println(rct.equals(bct));
		System.out.println(bct.equals(t2));
                System.out.println("<transitive>");
                System.out.println(rct.equals(t2));
                System.out.println(t2.equals(bct));
                System.out.println(rct.equals(bct));
                System.out.println("<consistent>");
                System.out.println(bct.equals(rct));
                System.out.println("<non-null>");
                System.out.println(rct.equals(null));
		System.out.println(t2.equals(null));
		System.out.println(bct.equals(null));
	
        	 System.out.println("--Equals _ version 3");

Ch4Triangle1 t4 = new Ch4Triangle1(new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle3 rct2 = new Ch4ColoredTriangle3(Color.red,new Point(0,0),new Point(1,1),new Point(2,2));
Ch4ColoredTriangle3 bct2 = new Ch4ColoredTriangle3(Color.blue,new Point(0,0),new Point(1,1),new Point(2,2));

		System.out.println("<reflexive>");
                System.out.println(t4.equals(t4));
                System.out.println(rct2.equals(rct2));
                System.out.println(bct2.equals(bct2));
                System.out.println("<symmetric>");
                System.out.println(t4.equals(rct2));
                System.out.println(rct2.equals(bct2));
                System.out.println(bct2.equals(t4));
                System.out.println("<transitive>");
                System.out.println(rct2.equals(t4));
                System.out.println(t4.equals(bct2));
                System.out.println(rct2.equals(bct2));
                System.out.println("<consistent>");
                System.out.println(bct2.equals(rct2));
                System.out.println("<non-null>");
                System.out.println(rct2.equals(null));
                System.out.println(t4.equals(null));
                System.out.println(bct2.equals(null));
		
System.out.println("\n--Refactoring");
		System.out.println("Room Charge : 1000");
		System.out.println("Meal Charge : 500");
		System.out.println("Movie Charge : 100\n");
		System.out.println("<getRoomCharge()>");
		Ch4Refactoring1 r1 = new Ch4Refactoring1();
		System.out.println("Room Charge is " + r1.getRoomCharge());
		System.out.println("<Rename method refactoring version>");
                Ch4Refactoring2 r2 = new Ch4Refactoring2();
                System.out.println("Total Bill is " + r2.getTotalBill());
		System.out.println("<Introduce explaining variable refactoring version>");
                Ch4Refactoring3 r3 = new Ch4Refactoring3();
                System.out.println("Room Charge is " + r3.getRoomCharge());
		System.out.println("Total Bill is " + r3.getTotalBill());
		System.out.println("<Replace temp with query refactoring version>");
		Ch4Refactoring4 r4 = new Ch4Refactoring4();
                System.out.println("Room Charge is " + r4.getRoomCharge());
		System.out.println("Meal Charge is " + r4.getMealCharge());
		System.out.println("Movie Charge is " + r4.getMovieCharge());
                System.out.println("Total Bill is " + r4.getTotalBill());
	}
	public static void ch3main(){
		System.out.println("--ch3 object");
		System.out.println("--ch3-MutableSquare");
		
		Ch3mutableSquare mu = new Ch3mutableSquare(4,7,9);
		Ch3mutableRectangle re = new Ch3mutableRectangle(4,7,8,6);
		System.out.println("Squarewidth = " +mu.getWidth()+","+"Squareheight =" +mu.getHeight());
		System.out.println("Rectanglewidth = " +re.getWidth()+","+"Rectangleheight=" +re.getHeight());

		System.out.println("--ch3-Person,Student");
		
		Ch3Person me = new Ch3Person("Hana Na","seoul");
		Ch3Student st = new Ch3Student(me,(float)4.5);
		System.out.println("Name = " +st.getName() + " GPA=" +st.getGPA());
		Ch3Person me2 = new Ch3Person("Somin Lee","seoul");
		Ch3Employee em = new Ch3Employee(me2,(float)10000.0);
		System.out.println("Name = " +em.getName() + " SALARY =" +em.getSalary());
		System.out.println("--ch3-Sorter");

		String[] B={"John","Adams","Skrien","Smith","Jones"};
		System.out.println("<before> ");
		for (int i=0; i<B.length; i++)
		{
			System.out.println(B[i]);
		}
		Ch3Comparator stringComp = new Ch3StringComparator();
		Ch3Sorter.sort(B,stringComp);
		System.out.println();
		System.out.println("<after>");
		for (int i=0; i<B.length; i++)
		{
			System.out.println(B[i]);
		}
		Integer[] C={new Integer(3), new Integer(1), new Integer(4), new Integer(2)};
		System.out.println();
		System.out.println("<before> ");
                for (int i=0; i<C.length; i++)
                {
                        System.out.println(C[i]);
                }

		Ch3Comparator integerComp = new Ch3IntegerComparator();
		Ch3Sorter.sort(C,integerComp);
		System.out.println();
                System.out.println("<after>");
		for (int i=0; i<C.length; i++)
		{
                	System.out.println(C[i]);
		}

	
	}

	public static void ch2main(){
		System.out.println("--ch2-object");
		System.out.println("--ch2-EnhancedRectangle and Oval and FilledOval");
			EnhancedRectangle rectangle = new EnhancedRectangle(1,2,50,60);			rectangle.setLocation(10,10);
	        	rectangle.setCenter(50,60);
		
			Graphic f = new Graphic();
			 f.setSize(150,200);
			 f.setVisible(true);
		
		System.out.println("--ch2-array vs linked");
			
			Listvs listvs = new Listvs(new ArrayList<String>());
			System.out.println("Add to List");
			listvs.addLi("A");
			listvs.addLi("B");
			listvs.addLi("C");
			listvs.iter();
			System.out.println("Size of List");
			listvs.sizeLi();
			System.out.println("Clear a List");
			listvs.clearLi();

		System.out.println("--ch2-Automobile Polymorphism");

			int totalCapacity = 0;
			Automobile[] fleet = new Automobile[3];
			fleet[0]=new Sedan(4);
			fleet[1]=new Minivan(9);
			fleet[2]=new SportsCar(3);
			for(int i=0; i<fleet.length; i++){
				if(fleet[i] instanceof Sedan)
					totalCapacity += ((Sedan) fleet[i]).getCapacity();
				else if (fleet[i] instanceof Minivan)
					totalCapacity +=((Minivan)fleet[i]).getCapacity();
				else if (fleet[i] instanceof SportsCar)
					totalCapacity +=((SportsCar)fleet[i]).getCapacity();
				else
					totalCapacity +=fleet[i].getCapacity();
			}
		System.out.println("Verson1.totalCapacity = " +totalCapacity);
			
			totalCapacity=0;
			for(int i=0; i<fleet.length; i++)
				totalCapacity +=fleet[i].getCapacity();
			System.out.println("Version2.totalCapacity="+totalCapacity);	
	}
}
