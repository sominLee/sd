package com.ex.ood;

import com.ex.ood.*;
import java.util.*;
import java.awt.*;

public class FilledOval extends Oval
{
	public FilledOval(int x, int y, int w, int h)
	{
		super(x,y,w,h);
	}
	public void draw(Graphics g)
	{
		g.fillOval(x,y,w,h);
	}
}
