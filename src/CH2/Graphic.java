package com.ex.ood;

import java.awt.*;
import java.awt.event.*;
import com.ex.ood.*;

public class Graphic extends Frame{
	Oval ov = new Oval (40,30,50,20);
	Oval ov2 = new Oval (100,50,20,50);
	FilledOval fov = new FilledOval(20,80,60,90);
	public void paint(Graphics g){
		ov.draw(g);
		ov2.draw(g);
		fov.draw(g);
	}

public Graphic(){
	addWindowListener(new WindowAdapter(){
	public void windowClosing(WindowEvent e){
	dispose();
	System.exit(0);
	}
});
}
}
