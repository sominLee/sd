package com.ex.ood;

import java.awt.*;

public class Oval 
{
	public int x, y, w, h;
	public Oval (int x, int y, int w, int h){
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
	}
	public void draw(Graphics g){
		g.drawOval(x,y,w,h);
	}
	public int getWidth() {
		return w;
	}
	public int getHeight() {
		return h;
	}
	public Point getTopLeftPoint(){
		return new Point(x,y);
	}
}

