package com.ex.ood;

import java.awt.Point;
import java.awt.Color;
import com.ex.ood.*;

public class Ch4ColoredTriangle2 extends Ch4Triangle1 {

        private Color color;

        public Ch4ColoredTriangle2(Color c,Point p1, Point p2, Point p3)
        {
                super(p1,p2,p3);
                if( c == null) c = Color.red;
                        color = c;
        }

        public boolean equals(Object obj)
        {
	   if(obj instanceof Ch4ColoredTriangle2)
           {
                Ch4ColoredTriangle2 otherColoredTriangle = (Ch4ColoredTriangle2)obj;
                return super.equals(otherColoredTriangle) &&
                        this.color.equals(otherColoredTriangle.color);
           }
           else if (obj instanceof Ch4Triangle1)
           {
                return super.equals(obj);
           }
           else
                return false;
        }
}


