package com.ex.ood;

public class Ch4Refactoring1 {
	public Ch4Refactoring1() {
	}
	public double getRoomCharge() {
		double roomCharge = 1000;
		double mealCharge = 500;
		double movieCharge = 100;
		return roomCharge + mealCharge + movieCharge;
	}
}
