package com.ex.ood;

import java.awt.Point;

public class Ch4Triangle2 {

        private Point p1,p2,p3;

        public Ch4Triangle2(Point p1,Point p2, Point p3)
        {
                if(p1 ==null) p1 = new Point(0,0);
                if(p2 ==null) p2 = new Point(0,0);
                if(p3 ==null) p3 = new Point(0,0);
                this.p1= p1;
                this.p2= p2;
                this.p3 =p3;
        }

        public boolean equals(Object obj)
        {
           if(obj == null) return false;
           if(obj.getClass() != this.getClass()) return false;

          Ch4Triangle2 otherTriangle =(Ch4Triangle2) obj ;
                return (p1.equals(otherTriangle.p1) &&
                        p2.equals(otherTriangle.p2) &&
                        p3.equals(otherTriangle.p3));
        }
}

