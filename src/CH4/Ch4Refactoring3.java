package com.ex.ood;

public class Ch4Refactoring3 {
        public Ch4Refactoring3() {
	}
        public double getRoomCharge() {
                return 1000;
        }
        public double getTotalBill() {
		final double roomCharge = getRoomCharge();
                double mealCharge = 500;
                double movieCharge = 100;
                return roomCharge + mealCharge + movieCharge;
        }

}

