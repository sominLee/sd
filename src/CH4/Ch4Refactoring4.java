package com.ex.ood;

public class Ch4Refactoring4 {
        public Ch4Refactoring4() {
        }
        public double getRoomCharge() {
                return 1000;
        }
	public double getMealCharge() {
                return 500;
        }
	public double getMovieCharge() {
                return 100;
        }
        public double getTotalBill() {
                return getRoomCharge() + getMealCharge() + getMovieCharge();
        }

}

