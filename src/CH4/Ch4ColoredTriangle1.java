
package com.ex.ood;
import java.awt.Point;
import java.awt.Color;
import com.ex.ood.*;

public class Ch4ColoredTriangle1 extends Ch4Triangle1 {

	private Color color;
	
	public Ch4ColoredTriangle1(Color c,Point p1, Point p2, Point p3)
	{
		super(p1,p2,p3);
		if( c == null) c = Color.red;
			color = c;
	}

	public boolean equals(Object obj)
	{
	 if( !(obj instanceof Ch4ColoredTriangle1))
		return false;
	   Ch4ColoredTriangle1 otherTriangle = (Ch4ColoredTriangle1) obj;
	   return super.equals(otherTriangle) && this.color.equals(otherTriangle.color); 
	}
}
