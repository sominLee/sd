package com.ex.ood;

import com.ex.ood.*;
import com.ex.ood.State;

public class GumballMachine{
	State soldOutState;
	State noQuarterState;
	State hasQuarterState;
	State soldState;

	State state = soldOutState;
	int count =0;

	public GumballMachine(int numberGumballs){
		soldOutState = new SoldOutState(this);
		noQuarterState = new NoQuarterState(this);
		hasQuarterState = new HasQuarterState(this);
		soldState = new SoldState(this);
		this.count = numberGumballs;

		if(numberGumballs > 0 ){
			state = noQuarterState;
		}

	}

	public void insertQuarter(){
		state.insertQuarter();
	}
	public void ejectQuarter(){
		state.ejectQuarter();
	}
	public void turnCrank(){
		state.turnCrank();
	//	state.dispense();
	}
	public void setState(State state){
		this.state = state;
	}
	public void releaseBall(){
		System.out.println("알맹이가 나가고 있습니다.");
		if(count !=0){
			count = count -1;
		}
	}

	public State getNoQuarterState(){
		state = noQuarterState;
		return this.state;
	}
	public State getSoldOutState(){
		return this.state;
	}
	public State getHasQuarterState(){
		state = hasQuarterState;
		return this.state;
	}
	public State getSoldState(){
		state = soldState;
		return this.state;
	}
	public int getCount(){
		return this.count;
	}
	public void print(){
		System.out.println("");
		System.out.println("주식회사 왕뽑기");
	        System.out.println("자바로 돌아가는 2004년형 뽑기 기계");
        	System.out.println("남은 개수 :"+ count + "개");

		if(count == 0)
			System.out.println("매진");
		else
	                System.out.println("동전 투입 대기중");
		System.out.println("");
	}
}
		
