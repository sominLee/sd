package com.ex.ood;

import com.ex.ood.*;
import java.util.ArrayList;

public abstract class Ch7Pizza{
	String name;
	String dough;
	String sauce;
	ArrayList topping = new ArrayList();
	
	void prepare(){
		System.out.println("Pizza Preparing..");
	}
	void bake(){
		System.out.println("backing..");
	}
	void cut(){
		System.out.println("cutting..");
	}
	void box(){
		System.out.println("Boxing..");
	}
	public String getName(){
		return this.name;
	}
}
