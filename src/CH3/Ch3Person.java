package com.ex.ood;

public class Ch3Person{

	private String name;
	private String address;
	public Ch3Person(String name, String address){
	
		this.name = name;
		this.address = address;
	}

	public String getAddress()
	{return address;}

	public String getName()
	{return name;}

	public void setAddress( String a)
	{
		this.address = a;
	}
	public void setName( String n)
	{
		this.name = n;
	}
}
