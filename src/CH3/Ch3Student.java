package com.ex.ood;

import com.ex.ood.Ch3Person;

public class Ch3Student{
	private Ch3Person me;
	private float myRecord;
	
	public Ch3Student(Ch3Person me,float myRecord){
		this.me = me;
		this.myRecord = myRecord;
	}
	public String getAddress()
	{return me.getAddress();}
	
	public float getGPA() {
		return myRecord;	
	}
	public String getName()
	{return me.getName();}
 
	public void setGPA(float myRecord){
		this.myRecord = myRecord;
	}
}

