package com.ex.ood;

import com.ex.ood.Ch3Rectangle;

public class Ch3mutableSquare extends Ch3Rectangle {
	public Ch3mutableSquare (int x, int y, int w){
		super(x,y,w,w);
	}
	public void setSize(int w) {
		super.setwidth(w);
		super.setheight(w);
	}
}
