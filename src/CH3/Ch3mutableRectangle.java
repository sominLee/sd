package com.ex.ood;

import com.ex.ood.Ch3Rectangle;

public class Ch3mutableRectangle extends Ch3Rectangle {

	public Ch3mutableRectangle (int x, int y, int w, int h){
		super(x,y,w,h);
	}
	public void setSize(int w, int h){
		super.setwidth(w);
		super.setheight(h);
	}
}
