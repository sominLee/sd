package com.ex.ood;

import com.ex.ood.Ch3Comparator;

public class Ch3Sorter{
	public static void sort(Object[] data, Ch3Comparator comp) {
		for (int i=data.length-1; i>=1; i--){
			int indexOfMax=0;
		for (int j=1; j<=i; j++){
			if(comp.compare(data[j],data[indexOfMax])>0)
				indexOfMax=j;
		}
	Object temp = data[i];
	data[i] = data[indexOfMax];
	data[indexOfMax] =temp;
		}
	}
}
