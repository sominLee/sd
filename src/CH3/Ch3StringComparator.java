package com.ex.ood;

import com.ex.ood.Ch3Comparator;

public class Ch3StringComparator implements Ch3Comparator{
	public int compare(Object o1, Object o2){
		String s1=(String) o1;
		String s2=(String) o2;
		return s1.compareTo(s2);
	}
}
