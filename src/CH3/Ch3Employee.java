package com.ex.ood;

import com.ex.ood.Ch3Person;

public class Ch3Employee{
	private Ch3Person me;
	private float salary;

	public Ch3Employee(Ch3Person me, float salary){
		this.me = me;
		this.salary = salary;
 	}
	public float getSalary()
	{ return salary;}

	public String getName()
	{ return me.getName();}
	
	public void setSalary(float salary) {
		this.salary = salary;
	}
}
