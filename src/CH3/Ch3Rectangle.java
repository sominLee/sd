package com.ex.ood;

public class Ch3Rectangle {
	private int x,y,width, height;
	public Ch3Rectangle (int x, int y, int w, int h) {
		this.x=x;
		this.y=y;
		width=w;
		height=h;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public int getArea() {
		return width * height;
	}
	public int getPerimeter() {
		return 2 * (width + height);
	}
//	public void setSize(int w, int h){
//		width =w;
//		height =h;
//	}
	public void setwidth(int w){
		this.width =w ;
	}
	public void setheight(int h) {
		this.height =h;
	}
	public void setTopLeft(int newx, int newy) {
		x=newx;
		y=newy;
	}

}	
