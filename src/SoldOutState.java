package com.ex.ood;

import com.ex.ood.*;

public class SoldOutState implements State{
	GumballMachine gumballMachine;

	public SoldOutState (GumballMachine gumballMachine){
		this.gumballMachine = gumballMachine;
	}
        public void insertQuarter(){
               System.out.println("매진되었습니다.");
        }
        public void ejectQuarter(){
                System.out.println("동전이 반환됩니다.");
        }
        public void turnCrank(){
                System.out.println("매진되었습니다.");
        }
        public void dispense(){
                System.out.println("알맹이가 나갈 수 없습니다.");
        }
}


