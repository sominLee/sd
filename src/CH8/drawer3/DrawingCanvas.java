package com.ex.ood.ch8.drawer3;

import java.awt.*;
import com.ex.ood.ch8.drawer3.figure.*;
import javax.swing.*;
import com.ex.ood.ch8.drawer3.*;
import java.util.Iterator;

public class DrawingCanvas extends JPanel{
	private Drawing drawing;
	public DrawingCanvas(){
		this.drawing = new Drawing();
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for (Figure figure : drawing){
			figure.draw(g);
		}
}
	public void addFigure(Figure newFigure){
		drawing.addFigure(newFigure);
		repaint();
	}
}
