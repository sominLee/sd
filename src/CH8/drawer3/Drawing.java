package com.ex.ood.ch8.drawer3;

import com.ex.ood.*;
import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import com.ex.ood.ch8.drawer3.figure.*;

public class Drawing implements Iterable<Figure>{
	private List<Figure> figures;
	public Drawing(){
		figures = new ArrayList<Figure>();	
	}
	public void addFigure(Figure newFigure){
		figures.add(newFigure);
	}
	public Iterator<Figure> iterator(){
		return figures.iterator();
	}
}
