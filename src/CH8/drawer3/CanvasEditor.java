package com.ex.ood.ch8.drawer3;

import com.ex.ood.ch8.drawer3.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import com.ex.ood.ch8.drawer3.figure.*;

public class CanvasEditor implements MouseListener{

/*
	private JButton currentButton;

	public CanvasEditor(JButton initialButton) {
		this.currentButton = initialButton;
	}
	public void actionPerformed(ActionEvent ae){
                currentButton = (JButton) ae.getSource();
        }
        public void mouseClicked(MouseEvent e){
                int x = e.getX();
        	int y = e.getY();
	
		JPanel canvas = (JPanel)e.getSource();
		if(currentButton.getText().equals("Ellipse"))
			canvas.getGraphics().drawOval(x-30,y-20,60,40);
		else if(currentButton.getText().equals("Rect"))
			canvas.getGraphics().drawRect(x-30,y-20,60,40);
		else
			canvas.getGraphics().drawRect(x-25,y-25,50,50);
	}
*/
	private Figure currentFigure;
	public CanvasEditor(Figure figure){
		this.currentFigure = figure;
	}
	public void setCurrentFigure(Figure newFigure){
		currentFigure = newFigure;
	}
	public void mouseClicked(MouseEvent e){
	/*
		JPanel canvas = (JPanel) e.getSource();
		currentFigure.setCenter(e.getX(),e.getY());
		currentFigure.draw(canvas.getGraphics());*/
		
		Figure newFigure = (Figure) currentFigure.clone();
		newFigure.setCenter(e.getX(),e.getY());
		((DrawingCanvas) e.getSource()).addFigure(newFigure);
	}

	public void mousePressed(MouseEvent m){

	}
	public void mouseReleased(MouseEvent m){

	}
	public void mouseEntered(MouseEvent m){

	}
	public void mouseExited(MouseEvent m){

	}

}
