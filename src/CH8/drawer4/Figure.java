package com.ex.ood.ch8.drawer4.figure;
import java.awt.*;

public abstract class Figure implements Cloneable {

    private Rectangle boundingRect; // 선택영역
    private boolean isSelected;

    public Figure(int x, int y, int w, int h)     {
        this.boundingRect = new Rectangle(x, y, w, h);
        isSelected = false;
    }

    public void moveTo(int x, int y)     {
        boundingRect.x = x;
        boundingRect.y = y;
    }

    public void move(int dx, int dy)     {
        boundingRect.x += dx;
        boundingRect.y += dy;
    }

    /**
     * @return the smallest Rectangle bounding this figure
     */
    public Rectangle getBoundingRect()     {
        return new Rectangle(boundingRect);
    }

    public boolean contains(int px, int py)     {
        return boundingRect.contains(px, py);
    }

    public Object clone()     {
        try {
            Figure clone = (Figure) super.clone();
            clone.boundingRect = (Rectangle) boundingRect.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            assert false; //this block should never execute
            return null;
        }
    }

    public boolean intersects(Rectangle selectionRect)     {
        return boundingRect.intersects(selectionRect);
    }

    public void setSelected(boolean b)     {
        isSelected = b;
    }

    public boolean isSelected()     {
        return isSelected;
    }

    /**
     * draws this figure, including highlighting
     * @param g the Graphics object that does the drawing
     */
    public void draw(Graphics g)     {
        drawShape(g);
        if (isSelected)
            highlightBoundingRect(g);
    }

    private void highlightBoundingRect(Graphics g)     {
        Color oldColor = g.getColor();
        g.setColor(Color.green);
        int x = boundingRect.x;
        int y = boundingRect.y;
        int w = boundingRect.width;
        int h = boundingRect.height;
        g.drawRect(x - 3, y - 3, 6, 6);
        g.drawRect(x + w - 3, y - 3, 6, 6);
        g.drawRect(x - 3, y + h - 3, 6, 6);
        g.drawRect(x + w - 3, y + h - 3, 6, 6);
        g.setColor(oldColor);
    }

    /**
     * draws the underlying shape ignoring highlighting
     * @param g the Graphics object that does the drawing
     */
    public abstract void drawShape(Graphics g);
}

 
