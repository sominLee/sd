package com.ex.ood.ch8.drawer4.figure;

import java.awt.*;

public class Square extends Rect{
	public Square(int centerX, int centerY, int w){
		super(centerX,centerY,w,w);
	}
  public void drawShape(Graphics g)     {
        Rectangle rect = getBoundingRect();
        g.drawRect(rect.x, rect.y, rect.width, rect.width);
    }

}
