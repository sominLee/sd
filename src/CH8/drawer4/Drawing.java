package com.ex.ood.ch8.drawer4;

import com.ex.ood.*;
import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import com.ex.ood.ch8.drawer4.figure.*;

public class Drawing implements Iterable<Figure>
{
	private List<Figure> figures;
	
	    public Drawing()  {
        figures = new ArrayList<Figure>();
    }

    //---------- adding/finding figures ------------

    //precondition:  newFigure is non-null and is not already in
    //this Drawing
    public void addFigure(Figure newFigure)  {
        figures.add(newFigure);
    }

    public Iterator<Figure> iterator() {
        return figures.iterator();
    }

    public Figure getFigureContaining(int x, int y) {
        for (int i = figures.size() - 1; i >= 0; i--) {
            Figure figure = figures.get(i);
            if (figure.contains(x, y))
                return figure;
        }
        return null;
    }
 public void selectFigure(Figure figure) {
        figure.setSelected(true);
    }

    public void selectAll() {
        for (Figure figure : figures) {
            figure.setSelected(true);
        }
    }

    public void unselectAll() {
        for (Figure figure : figures) {
            figure.setSelected(false);
        }
    }

    public void selectIntersectingFigures(Rectangle selectionRect) {
        for (Figure figure : figures) {
            if (figure.intersects(selectionRect)) {
                figure.setSelected(true);
            }
            else {
                figure.setSelected(false);
            }
        }
    }

    public void moveSelectedFigures(int dx, int dy) {
        for (Figure figure : figures) {
            if (figure.isSelected()) {
                figure.move(dx, dy);
            }
        }
    }
}

