package com.ex.ood.ch8.drawer4.figure;

import java.awt.*;

public class Ellipse extends Figure{
	public Ellipse(int centerX,int centerY, int w, int h){
		super(centerX,centerY,w,h);
	}

	public void drawShape(Graphics g)     {
        Rectangle rect = getBoundingRect();
        g.drawOval(rect.x, rect.y, rect.width, rect.height);
    }

}
