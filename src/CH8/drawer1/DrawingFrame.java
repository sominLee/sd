package com.ex.ood.ch8.drawer1;

import com.ex.ood.*;
import javax.swing.*;
import java.awt.*;

/**
	*@author Dale Skrien
	*@version 1.0 August 2005
*/
public class DrawingFrame extends JFrame{
	public DrawingFrame(){
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JComponent drawingCanvas = createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);

		JToolBar toolbar = createToolbar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}
/**
        *creates the canvas (the part of the window below the tool bar)
        *@return the new JComponent that is the canvas
*/

	public JComponent createDrawingCanvas(){
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}

/**
	*creates the tool bar with the three buttons
	*@return the new tool bar
*/
	private JToolBar createToolbar(JComponent canvas){
		JToolBar toolbar = new JToolBar();
		JButton ellipseButton = new JButton("Ellipse");
		toolbar.add(ellipseButton) ;
		JButton squareButton = new JButton("Square");
		toolbar.add(squareButton);
		JButton rectButton = new JButton("Rect");
		toolbar.add(rectButton);
		
		CanvasEditor canvasEditor = new CanvasEditor(ellipseButton);
		ellipseButton.addActionListener(canvasEditor);
		squareButton.addActionListener(canvasEditor);
		rectButton.addActionListener(canvasEditor);
		canvas.addMouseListener(canvasEditor);
		
		return toolbar;
	}

/**
	*creates and displays a DrawingFrame object
	*@param args
*/

	
}
