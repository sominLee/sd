package com.ex.ood.ch8.drawer2.figure;

import java.awt.*;

public class Ellipse extends Figure{
	public Ellipse(int centerX,int centerY, int w, int h){
		super(centerX,centerY,w,h);
	}

	public void draw(Graphics g){
		int width = getWidth();
		int height = getHeight();
		g.drawOval(getCenterX() - width/2,getCenterY() - height/2,width,height);
	}
}
