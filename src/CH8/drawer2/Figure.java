package com.ex.ood.ch8.drawer2.figure;
import java.awt.*;

public abstract class Figure{

	private int centerX, centerY;
	private int width;
	private int height;
	public Figure(int centerX,int centerY,int width,int height){
		this.centerX = centerX;
		this.centerY = centerY;
		this.width = width;
		this.height = height;
	}
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	public void setCenter(int centerX,int centerY){
		this.centerX = centerX;
		this.centerY = centerY;
	}
	public int getCenterX(){return centerX;}
	public int getCenterY(){return centerY;}
	
	public abstract void draw(Graphics g);
}
 
