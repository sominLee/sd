package com.ex.ood;

import com.ex.ood.*;

public class GumballMachineTestDrive{
	public static void main(String[] args){
		GumballMachine gumballMachine = new GumballMachine(5);
		
		gumballMachine.print();

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		
		gumballMachine.print();
	
		gumballMachine.insertQuarter();
		gumballMachine.ejectQuarter();
		gumballMachine.turnCrank();

 		gumballMachine.print();

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
                gumballMachine.insertQuarter();
                gumballMachine.turnCrank();
                gumballMachine.ejectQuarter();

                gumballMachine.print();


                gumballMachine.insertQuarter();
                gumballMachine.insertQuarter();
                gumballMachine.turnCrank();
                gumballMachine.insertQuarter();
                gumballMachine.turnCrank();
                gumballMachine.insertQuarter();
                gumballMachine.turnCrank();

                gumballMachine.print();

	}
}			

